package com.czina.cdidemo.ui;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.UnsatisfiedResolutionException;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

import com.czina.cdidemo.cdi.command.CmdAnnotationInstance;
import com.czina.cdidemo.cdi.command.Command;

@ApplicationScoped
@Named("terminalController")
public class TerminalController {
	
	@Inject
	private Logger logger;

	@Inject
	@Any
	private Instance<Command> commandInstance;

	public String handleCommand(String command, String[] params) {
		StringBuffer output = new StringBuffer();

		try {
		Command commandBean = commandInstance.select(
				new CmdAnnotationInstance(command)).get();
		output.append(commandBean.handleCommand(command, params));
		} catch(UnsatisfiedResolutionException e) {
			output.append("SYNTAX ERROR: ").append(e.getMessage());
			logger.error("SYNTAX ERROR!", e);
		}

		return output.toString();
	}
}
