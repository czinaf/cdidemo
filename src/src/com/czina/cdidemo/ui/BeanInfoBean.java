package com.czina.cdidemo.ui;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

import com.czina.cdidemo.cdi.api.BeanInfo;

@Model
@Named("beanInfoBean")
public class BeanInfoBean {

	@Inject
	private Logger logger;

	@Inject
	@Any
	private Instance<BeanInfo> beanInfoInstances;

	private List<BeanInfo> beanInfoList;

	public Instance<BeanInfo> getBeanInfoInstances() {
		return beanInfoInstances;
	}

	public List<BeanInfo> getBeanInfoList() {
		beanInfoList = new ArrayList<BeanInfo>();
		for (BeanInfo beanInfo : beanInfoInstances) {
			beanInfoList.add(beanInfo);
		}
		return beanInfoList;
	}

}
