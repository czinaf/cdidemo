package com.czina.cdidemo.ui;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

import com.czina.cdidemo.cdi.ApplicationBean;
import com.czina.cdidemo.cdi.SessionBean;

@Model
@Named("enterBean")
public class EnterBean {

	@Inject
	private Logger logger;

	@Inject
	private SessionBean sessionBean;

	@Inject
	private ApplicationBean applicationBean;

	public String getSessionText() {
		return sessionBean.getSessionText();
	}

	public void setSessionText(String value) {
		sessionBean.setSessionText(value);
	}

	public String getApplicationText() {
		return applicationBean.getSavedState();
	}

	public void setApplicationText(String value) {
		applicationBean.setSavedState(value);
	}

	public void save() {
		logger.info("Save index page");
	}
}
