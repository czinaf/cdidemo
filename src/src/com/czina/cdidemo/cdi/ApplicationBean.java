package com.czina.cdidemo.cdi;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

import com.czina.cdidemo.cdi.api.BeanInfo;

@ApplicationScoped
@Named("applicationBean")
public class ApplicationBean extends BeanInfo {

	@Inject
	private Logger logger;

	private String savedState;

	@Inject
	private SessionBean sessionBean;

	public String getSavedState() {
		return savedState;
	}

	public void setSavedState(String savedState) {
		this.savedState = savedState;
	}

	public String getSessionContent() {
		return this.sessionBean.getSessionText();
	}

}
