package com.czina.cdidemo.cdi.api;

public interface Decorable {
	
	public void doSomething(String input);

}
