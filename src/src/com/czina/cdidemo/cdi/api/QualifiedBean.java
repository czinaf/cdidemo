package com.czina.cdidemo.cdi.api;

public abstract class QualifiedBean {
	
	public String getName() {
		return this.getClass().getSimpleName();
	}

}
