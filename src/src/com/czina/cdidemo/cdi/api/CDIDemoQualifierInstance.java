package com.czina.cdidemo.cdi.api;

import javax.enterprise.util.AnnotationLiteral;

public class CDIDemoQualifierInstance extends
		AnnotationLiteral<CDIDemoQualifier> implements CDIDemoQualifier {

	private static final long serialVersionUID = 8941130307568669145L;

	private String subType;
	
	public CDIDemoQualifierInstance(String subType) {
		this.subType = subType;
	}
	
	@Override
	public String value() {
		return subType;
	}

}
