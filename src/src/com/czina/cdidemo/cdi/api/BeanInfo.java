package com.czina.cdidemo.cdi.api;

import java.util.Random;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;

public abstract class BeanInfo {

	@Inject
	private Logger logger;
	private String applicationId;

	@PostConstruct
	private void postConstruct() {
		applicationId = String.valueOf((new Random()).nextInt(200000));
		logger.info("Creating {} with id: {}", getBeanName(), this.applicationId);
	}

	public String getApplicationId() {
		return applicationId;
	}

	public String getBeanName() {
		return this.getClass().getSimpleName();
	}
}
