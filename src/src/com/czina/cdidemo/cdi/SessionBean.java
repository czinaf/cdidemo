package com.czina.cdidemo.cdi;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

import com.czina.cdidemo.cdi.api.BeanInfo;

@SessionScoped
public class SessionBean extends BeanInfo implements Serializable {

	private static final long serialVersionUID = 2968899012586733763L;

	@Inject
	private Logger logger;

	private String sessionText;

	public String getSessionText() {
		return sessionText;
	}

	public void setSessionText(String sessionText) {
		logger.info("Set sessionText to new value: {}", sessionText);
		this.sessionText = sessionText;
	}

}
