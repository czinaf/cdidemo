package com.czina.cdidemo.cdi.decorator;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

import org.slf4j.Logger;

import com.czina.cdidemo.cdi.api.Decorable;

@Decorator
public abstract class TestDecoratorBean implements Decorable {

	@Inject
	private Logger logger;
	
	@Inject @Delegate @Any
	private Decorable delegating;
	
	@Override
	public void doSomething(String input) {
		logger.info("I'm delegating invocation (parameter: {})", input);
	}

	
}
