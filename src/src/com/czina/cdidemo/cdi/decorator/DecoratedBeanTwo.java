package com.czina.cdidemo.cdi.decorator;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.czina.cdidemo.cdi.api.Decorable;

public class DecoratedBeanTwo implements Decorable {

	@Inject
	private Logger logger;
	
	@Override
	public void doSomething(String input) {
		logger.info("Hey, I am {} and I have just invoked.", this.getClass().getSimpleName());
	}

}
