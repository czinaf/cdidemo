package com.czina.cdidemo.cdi.command;

import javax.enterprise.util.AnnotationLiteral;

public class CmdAnnotationInstance extends AnnotationLiteral<Cmd> implements
		Cmd {

	private static final long serialVersionUID = 8941630307578669145L;

	private String handledCommand;

	public CmdAnnotationInstance(String handledCommand) {
		this.handledCommand = handledCommand;
	}

	@Override
	public String value() {
		return handledCommand;
	}

}
