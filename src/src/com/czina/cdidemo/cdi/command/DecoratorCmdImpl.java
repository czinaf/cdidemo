package com.czina.cdidemo.cdi.command;

import javax.inject.Inject;

import com.czina.cdidemo.cdi.api.BeanInfo;
import com.czina.cdidemo.cdi.decorator.DecoratedBeanOne;
import com.czina.cdidemo.cdi.decorator.DecoratedBeanTwo;

@Cmd("delegate")
public class DecoratorCmdImpl implements Command {

	
	@Inject
	private DecoratedBeanOne decoratedBeanOne;
	
	@Inject
	private DecoratedBeanTwo decoratedBeanTwo;
	@Override
	public String handleCommand(String command, String[] params) {
		decoratedBeanOne.doSomething(params[0]);
		decoratedBeanTwo.doSomething(params[1]);
		return "Decorated";
	}

	@Override
	public String buildHelpString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Decorator: delegate FIRSTPARAM SECONDPARAM");
		return sb.toString();
	}
}
