package com.czina.cdidemo.cdi.command;

import javax.inject.Inject;

import com.czina.cdidemo.cdi.ApplicationBean;
import com.czina.cdidemo.cdi.api.BeanInfo;

@Cmd("content")
public class ContentCmdImpl implements Command {

	@Inject
	private ApplicationBean applicationBean;

	@Override
	public String handleCommand(String command, String[] params) {
		StringBuffer output = new StringBuffer();
		if (params[0].equals("session")) {
			output.append("Session content: ").append(
					applicationBean.getSessionContent());
		} else if (params[0].equals("application")) {
			output.append("Application content: ").append(
					applicationBean.getSavedState());
		}
		return output.toString();
	}

	@Override
	public String buildHelpString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Content: content {session|application}");
		return sb.toString();
	}

}
