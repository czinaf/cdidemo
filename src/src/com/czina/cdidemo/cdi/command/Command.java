package com.czina.cdidemo.cdi.command;

public interface Command {

	public String handleCommand(String command, String[] params);

	public String buildHelpString();
}
