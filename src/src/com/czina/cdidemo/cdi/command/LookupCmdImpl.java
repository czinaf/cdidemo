package com.czina.cdidemo.cdi.command;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.czina.cdidemo.cdi.api.CDIDemoQualifier;
import com.czina.cdidemo.cdi.api.CDIDemoQualifierInstance;
import com.czina.cdidemo.cdi.api.QualifiedBean;

@Cmd("lookup")
public class LookupCmdImpl implements Command {

	@Inject
	@Any
	private Instance<QualifiedBean> qualifiedBeanInstance;

	@Inject
	@CDIDemoQualifier("ONE")
	private QualifiedBean qualifiedBeanOne;

	@Inject
	@CDIDemoQualifier("TWO")
	private QualifiedBean qualifiedBeanTwo;

	@Inject
	@CDIDemoQualifier("THREE")
	private QualifiedBean qualifiedBeanThree;
	
	@Override
	public String handleCommand(String command, String[] params) {
		StringBuffer output = new StringBuffer();

		String beanName = "NOT FOUND!";
		if ("static".equals(params[0])) {
			switch (params[1]) {
			case "ONE": {
				beanName = qualifiedBeanOne.getName();
				break;
			}
			case "TWO": {
				beanName = qualifiedBeanTwo.getName();
				break;
			}
			case "THREE": {
				beanName = qualifiedBeanThree.getName();
				break;
			}
			default: {
				beanName = "WRONG PARAMETER";
				break;
			}
			}
		} else if ("dynamic".equals(params[0])) {
			QualifiedBean bean = qualifiedBeanInstance.select(
					new CDIDemoQualifierInstance(params[1])).get();
			beanName = bean.getName();
		}
		output.append("Qualified bean name: ").append(beanName);
		
		return output.toString();
	}
	
	@Override
	public String buildHelpString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Lookup: lookup {static|dynamic} {ONE|TWO|THREE}");
		return sb.toString();
	}

}
