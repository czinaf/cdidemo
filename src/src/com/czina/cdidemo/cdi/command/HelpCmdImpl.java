package com.czina.cdidemo.cdi.command;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

@Cmd("help")
public class HelpCmdImpl implements Command {

	@Inject
	@Any
	private Instance<Command> commandInstance;
	
	@Override
	public String handleCommand(String command, String[] params) {
		StringBuffer sb = new StringBuffer();
		for(Command commandImpl : commandInstance) {
			sb.append(commandImpl.buildHelpString()).append("</br>");
		}
		return sb.toString();
	}

	@Override
	public String buildHelpString() {
		return "";
	}

}
