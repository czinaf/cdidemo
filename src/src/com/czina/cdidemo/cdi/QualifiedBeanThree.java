package com.czina.cdidemo.cdi;

import com.czina.cdidemo.cdi.api.CDIDemoQualifier;
import com.czina.cdidemo.cdi.api.QualifiedBean;

@CDIDemoQualifier("THREE")
public class QualifiedBeanThree  extends QualifiedBean{

}
