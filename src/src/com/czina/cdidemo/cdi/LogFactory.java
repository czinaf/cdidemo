package com.czina.cdidemo.cdi;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogFactory {

	private static final Logger logger = LoggerFactory
			.getLogger(LogFactory.class);

	@Produces
	Logger createLogger(InjectionPoint injectionPoint) {
		
		Class<?> targetClass = injectionPoint.getMember()
				.getDeclaringClass();
		
		logger.info("Injecting logger for {}", targetClass.getSimpleName());
		return LoggerFactory.getLogger(targetClass);
	}
}
